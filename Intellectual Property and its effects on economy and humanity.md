
# <div align="center">Intellectual Property<br> and its effects on Economy and Humanity</div>

### <div align="center">No Copyright 2021 - CopyCat</div>

## Introduction
Movie piracy is stealing.  
We’ve all seen anti-piracy videos explaining that illegal downloads of movies are equal to theft. But is this statement actually true?
In order to answer, we have to dive into what lies behind this seemingly simple question: Intellectual property and the increasingly complicated process of patenting and copyright. As intellectual property is a rather complex concept, we will start off by looking at what it actually is and what it does.

## What is Intellectual Property?
Intellectual property is a concept which states that authors and inventors own their ideas. Intellectual property rights are exclusive, meaning that the owner of an idea can prohibit others from copying, using and improving upon it. It is very important to understand that intellectual property is not about artists and inventors owning their creation. An author automatically owns their work. If they have written a book, they own a copy of it and could do anything they wanted with it. Intellectual property laws are actually about the rights-holder owning every single copy of a work which means that the author also has the right to decide what all the other people who bought his work can or can't do with it.
Copyrights and patents are subcategories of intellectual property. Since they are the most controversial ones, they deserve a closer look and explanation as well.

#### What is Copyright?
A copyright gives the author of a creative work a monopoly over its distribution and sale and is given automatically. To name a few examples: books, movies, music, software, images, databases, dance choreographies and architecture are all covered by copyright. Copyright lasts for a limited period of time. In the United States it’s the life of the author + 70 years<sup>1</sup>. 

#### What are Patents?
Patents give an individual a monopoly for a limited time over the production and sale of an invention. Both new products (e.g. a more efficient battery) and new production techniques (e.g. the way a pharmaceutical company produces its drugs) can be patented  <sup>2</sup>. In contrast to copyright, patents are not given automatically; If someone wants to be granted a patent, they have to apply for one. In the United States this has to be done at the United States Patent and Trademark Office. Patents are usually quite costly, yet they only last for 20 years in most countries. An internationally acknowledged patent can cost up to 2.000.000 USD<sup>3</sup>. 

#### History of Copyright
Nowadays, the common understanding of copyright is that it exists to provide an incentive for authors to produce creative work. However, that is not the historic reason why it was created. Copyright was simply created as a tool of censorship for the government. After Gutenberg invented the printing press, influential people started becoming scared of the power that this invention yielded. It became possible to spread ideas at a significantly lower price and faster than ever before. Copyright was supposed to give back control to the government by allowing it to control the flow of information. In the beginnings, copyright gave authorized people a monopoly over printing books. This power was used to silence opposition and unwanted ideas. Many were victims of censorship through copyright - most notibly among them was the famous Galileo Galilei. The catholic church prevented him from spreading the idea of heliocentrism by prohibiting books on that topic to be printed<sup>4</sup>. It was only around the time of the French revolution when the idea that the product of creative work belonged to its author gained popularity. But the intention was the opposite of creating monopolies. The goal was to abolish the royal monopoly over copying books and to allow artists to reproduce their own creation without having to ask for anyone's permission. The first international copyright treaty was signed at the Berne convention in 1886<sup>5</sup>. Since then, copyrights have gotten stricter every year. Today it is possible to copyright things never imaginable before e.g. databases, dance moves, and even buildings. And it does not end here. In 2019, the European parliament decided to make copyright laws so strong that many activists and independent news reporters now fear losing their freedom of speech because of copyright<sup>6</sup>.

#### History of Patents
Just like copyright, the original purpose of patents was not to reward the innovator for his work but rather to enrich the government. Especially the British monarchy is well known for creating artificial monopolies. From the 17th. century on, royal patents were given out to citizens who traveled to other countries, bought inventions that didn't exist in Britain and brought them home<sup>7</sup>. For the monarchy, this was a great tool to profit from new inventions without having to develop them. Even more absurd: Britain sold monopolies on products that already existed<sup>8</sup>. The first step away from the arbitrarily given patents by the royals was the "Statute of Monopolies". It took influence away from the monarchy and gave it to actual inventors instead<sup>9</sup>. Since then, both the duration and the scope of what can be patented has increased every year.

### Clarifications
In this text we will analyze whether intellectual property helps our economy and society.
To be able to draw a conclusion, let us define the properties that intellectual property would need 
to benefit society:
1. It should increase innovation.
2. It should reward people proportionately to how much they innovate.
3. It should not have dangerous side effects.
4. Everybody should benefit from it; Nobody should be excluded.

When I am using the term “Pirate” in this text, I am not referring to people like captain Jack Sparrow, but to internet pirates - persons who illegally copy and spread culture and information online (for example by using file sharing systems and letting other people download movies and books from their computer).

Throughout this article I will be using the term “IP” very often, which is simply an abbreviation for “Intellectual Property”.

#### Immaterial Goods are Non Scarce
Before I start with my arguments I would like to resolve a few misconceptions regarding intellectual property.
First, the term “Intellectual Property” suggests that it has got something to do with property. But property is something completely different than intellectual property. 
Material resources are scarce. If i get a bigger piece of the cake, you will get less. Because of this, people would fight over resources if property rights didn't exist. So the justification for the existence for property rights is that they prohibit conflict. In contrast, immaterial goods (like an mp3 file on your PC) are abundant since they are infinitely reproducible and can be copied at no cost, which means that they are not scarce.
Intellectual property creates artificial scarcity by creating artificial monopolies. By sharing knowledge and ideas there can only be more information, never less. This is illustrated very well by the following quote
"If I have an apple and you have an apple, and we both exchange them, we still both have an apple, but if we both have an idea and we exchange them, we both now have two ideas". Everybody can own more information without somebody else having less. The quote "If everybody owns everything, everybody owns nothing." might be true for material resources. But with immaterial resources the correct saying would be "if everybody owns everything, everybody owns everything". Supporters of copyright like to argue that copying content is stealing or “Piracy”. So let’s compare the two with an example:

#### Comparison: Pirating a Ship vs "Pirating" a Movie

##### Pirating a Ship                      
- Owner has damages, loses his ship.
- Control over ship taken violently.
- Can endanger lives
- No value added, ownership of the ship was transferred to pirate.

##### Pirating a Movie
- Owner has no damages, he still gets to keep the movie for himself.
- No use of violence
- Nobody endangered  
- Value added, now one person more owns the movie.

## Does Intellectual Property Stimulate Innovation?
After we've examined the history of both patents and copyright, I'd like to point out that intellectual property never played a role in the emergence of a new industry. The reason is that laws for a new industry sector can only be written once an industry already exists and has established itself. A great example is the software industry, which started out in about 1960, for a long time, software couldn't be patented since it was a new kind of product. it took until 1981<sup>10</sup> where it became possible to patent software. But in the beginning these patents were rarely used. If patents were adopted faster in new industries, that would have had fatal consequences. Here is a quote from Bill Gates on the topic: "If people had understood how patents would be granted when most of today's ideas were invented, and had taken out patents, the industry would be at a complete standstill today"<sup>12</sup>. - Quite ironic that Bill Gates’ company Microsoft is well known for suing other companies for violating Microsoft’s software patents. Most of the innovation in an industry happens in the beginning where intellectual property doesn't exist. It is only when an industry matures that the ones who dominate the industry crave for monopoly because they want to keep their strong position in the market and keep making money the way they're used to. They achieve this by influencing politicians through expensive lobbying<sup>12</sup>.

In many industries, market leaders have realized that patents are harmful for them. In 1870 already, big players in the steel industry decided to share their patents with each other since they realized that innovation would come to a complete standstill otherwise. Since different companies had essential patents on crucial parts of steel production, none of the companies was able to improve their production process without violating another company's patent. So "Bessemer Steel" decided to create an association where the big steel companies could share their knowledge and would allow each other to use their patented processes and inventions<sup>13</sup>. Nowadays this practice is known as a "Patent Pool" where usually the largest companies of a sector allow each other to use their intellectual property without charge, thereby crating an oligopoly instead of a monopoly. While this is better for innovation than not having patent pools, newcomers to the industry are prevented from entering the market since they are not very likely to be able to compete with the association without violating their patents. 

Patenting a product usually involves not just one patent, but many more "defensive” patents to be able to defend against patents from other companies to use a new technology in many different ways. This happens very often as the Carnegie Survey from 2000 showed: The survey examines the reasons why companies choose to get a patent. 58.77% of companies defensively patent products to prevent lawsuits from other companies<sup>14</sup>. Many influential people in the computer industry have realized that the patents of other companies are preventing them from developing new products. Mr. Bruce Sewell from Intel who now works at Apple once said "We have 10,000 patents – it’s and awful lot of patents. Would I be happy with 1,000 patents rather than 10,000? Yes, provided the rest of the world did the same thing."<sup>15</sup> And Roger Smith from IBM said: "The IBM patent portfolio gains us the freedom to do what we need to do through cross-licensing — it gives us access to the inventions of others that are key to rapid innovation. Access is far more valuable to IBM than the fees it receives from its 9,000 active patents. There’s no direct calculation of this value, but it’s many times larger than the fee income,
perhaps an order of magnitude larger."<sup>16</sup> So he clearly points out that patents are not of importance for making money, but because they allow IBM to innovate without legal barriers. In a world without intellectual property, not a single company would have to worry about not having access to patents that are essential to produce and improve products.

Since there are several millions of patents, it is becoming harder every day for companies to keep track of every single new patent. This results in a situation where engineers are saying that they sometimes won't work on a new technology because they fear being sued by another company for using a patented method they were not aware about<sup>17</sup>.

Companies like Microsoft would like you to believe that IP creates an incentive for people to innovate. But there is actually no evidence that IP stimulates innovation and content creation. One of the first things we learned in economics class was: Monopolies are the last thing the economy needs. They create ridiculously high prices and because the monopolist who patents his products doesn't have competitors for 20 years, he doesn't have to improve his product to make it better. Monopolies produce less for more money.
In a world without intellectual property, it would be possible to compete with the monopolistic companies and as we all know: there is nothing better for the economy than competition.

To see if intellectual property motivates authors to create works, let’s take a look at the development of copyright. The duration of copyright more than doubled between 1900 and 2000, so if copyright actually creates an incentive for artists to create, then we should see a significant increase in the amount of literary output. Here is a graph showing the literary output per person in the United States per year.

![Yearly registered literature per person in the United States](LiteraryOutputUS.png)
<div align="center">[18]</div>

The red lines represent years where the length of copyright was extended. But as we can see, not a single extension of copyright seems to have had any effect on the amount of literacy produced. The graph shows fluctuations which don’t seem to suddenly change into another direction once the length of copyright was extended. So it doesn’t seem like authors decide whether or no to write a book based on the length of their copyright.

Next, I’m going to talk about the agricultural industry in the United States. For about 12.000 years, the agricultural sector developed without the existence of patents. Until 1970 it was not possible to patent many living beings including seeds like corn<sup>19</sup>. The first patent on a corn seed was filed in 1974<sup>20</sup>. Until then, the industry was healthily growing each year. But as soon as the industry became so influential that it could influence congress by lobbying, it tried to monopolize it’s business. One measure of the innovation in the agricultural sector is the yield in bushels per acre. IP supporters justify patents because they say that they stimulate innovation. So one would expect innovation to increase as soon as patents on plants were introduced.

![U.S. Corn Yield in Bushels per Acre](CornYieldUS.png)
<div align="center">[21]</div>

The green line represents the growth rate of the yield in bushels per acre in the decades before patents on corn seeds existed. The dashed green line shows how the graph should have continued if patents on corn didn’t have any effect on innovation at all while the purple line shows how the growth rate actually developed. And as we can see in this example, it doesn’t look like patents had a positive influence on innovation in the agricultural sector at all, it even seems like they have slowed down innovation since the purple line is below the dashed green line.
You may ask yourself how breeders were able to make money from their seeds without patents. Researching and developing a new seed is expensive and the company developing it has to make money to compensate for their expenses. If a breeding company creates a new type of seed, the seed is very scarce at first. This means that the company can sell the seeds for very high prices. The money made from the sale of the first seeds is enough for them to compensate for their research. An innovator is automatically rewarded, no matter if he has got a patent or not. Because the first one to come up with a successful idea will always be the first to sell the product. Thereby giving him a huge head start and opportunity to become reputable without competition.
This is very well illustrated by this diagram of supply and demand.

![Supply and Demand Diagram](PriceQuantity.png)

The diagram shows the red demand and blue supply graph. On the x-axis we can see the quantity of a product and on the y-axis it's price. The dashed vertical line is called capacity. This line shows which quantity the market is currently able to produce. The profit of a company is represented by the length of the piece of the capacity line that is between the demand and supply graph. When a new market emerges, the capacity begins at 0 and then increases slowly (for example: from situation A to situation B). As the capacity grows, the rents of producers decrease, which lowers the product’s cost to the marginal price. This means that the first company to sell a product will have the highest rent. So competitive markets reward innovators automatically. 

Especially Monsanto is known for abusing the patent system to maximize its profits, Monsanto has made it illegal to replant it’s seeds. Since thousands of years, farmers used the seeds which their own plants produce for the next year, this ensured that they could stay independent and made their business model sustainable. Monsanto doesn't like the fact that farmers have to buy a seed only once and are then able to produce as many as they want on their own. So the company has created a new business model which makes farmers dependent on it. Monsanto forces farmers to pay a license fee for every year they use Monsanto's seeds. Hundreds of farmers who didn't accept Monsanto’s abuse of the patent system were sued by the company. Farmers challenged Monsanto in court, but the company’s practices are legal. Monsanto has already sued farmers who replanted their own seeds for a total of 23.000.000$<sup>22</sup>. What many people are upset about is that companies like Monsanto never invented the seeds they sell. The seeds we have nowadays were developed by farmers over thousands of years in a collective effort<sup>23</sup>. Monsanto takes the product of the work of generations, makes marginal changes to it and is then is able to patent it.

Finally, three economists: Arundel, Gallini and Jaffa investigated a total of 24 Studies. These studies examined the effect of patents on the post second world war economies, from the 1980s on. And they came to the conclusion that not a single one of the studies found evidence suggesting a correlation between an increase in innovation and patents.

![Empirical Studies on Patents and Innovation](EmpiricalStudiesOnPatentsAndInnovation.png)
<div align="center">[24]</div>

## The Pharmaceutical Industry
The pharmaceutical industry seen by many supporters of intellectual property as a proof that innovation cannot take place without patents. They argue that researching and developing new drugs is very expensive and that pharmaceutical companies have to get paid for their work, otherwise we wouldn't see any new drugs. But first doubts regarding this narrative come up when looking at the history of drug patents. The general trend seems to to be that in the past, the countries without patent protection on drugs were way more competitive than the countries with pharmaceutical patents.
Italy’s pharmaceutical industry was and is one of the most profitable. In 2018, it even became the largest seller of drugs in the European Union. For a long time, patents on drugs were non-existent in Italy. At that time the Italian pharmaceutical companies were way more competitive than the ones in countries where patents existed. In Italy, patents on medicines were introduced in 1978. Let’s compare the amount of newly discovered chemical compounds in the 20 years before the introduction of patents in with the 20 years after the introduction. Before, Italy discovered about 9.28% of the new chemical compounds, after the introduction of drug patents, Italy discovered about 7.5%<sup>25</sup>. So it doesn’t look like patents increased innovation, it seems like they even had a negative effect on it. These numbers prove that when patents didn't exist in Italy, the pharmaceutical companies didn't simply imitate the drugs from other countries and sell them cheaper, they actually researched and developed more than many countries with patents. 

When pharmaceutical companies research drugs, we should expect that they are developing new ones, however, a study examining drugs between 1989 and 2000 showed that only 35% of the drugs developed by pharmaceutical companies actually have new active ingredients<sup>26</sup>. The rest of these drugs accounts for the so called "metoo" drugs. Metoo drugs are simply drugs that are redundant, they already exist. but because so many drugs are patented nowadays, other pharmaceutical companies who want to compete have to create their own drug to not violate any patent. So what they do is the following: They copy a drug from another company and modify it in such a way that the patent office accepts it as a new drug even though the difference is so small, that it often can't even be noticed. The money used for this questionable research has to be paid by someone. That someone are we, the tax payers. Pharmaceutical companies like to present themselves as the genius minds who make it possible to create new medicine that heals sicknesses that were not curable before. But actually, large parts of the funding for research don’t come from the pharmaceutical companies but from the public. The government of the United States spent over 30 billion USD in 2015 on researching new drugs<sup>27</sup>. A lot of the work happens at publicly funded universities where essential research for the development of new drugs is made. Pharmaceutical companies only step in in the last phase where the drug needs to be tested and finished into a final product.
One thing that pharmaceutical companies don’t like to talk about is that every day people die because of their patents. For example: Drugs for AIDS patients, these are very cheap to produce, but because pharmaceutical companies have patented them, they charge immense amounts of money. Millions of people in poorer countries like India and large parts of Africa cannot afford the expensive medicine which results in thousands of deaths. What’s even more problematic is that AIDS drugs were not even developed by the pharmaceutical industry but mainly by the academic researcher David Ho, for which he was named "person of the year" by TIME Magazine in 1996<sup>28</sup>. People die not only in developing countries but also in western nations like the U.S. because of too high drug prices. A study at Havard Medical School revealed that every year 45.000 Americans die because they can't afford the medicine they need to stay alive<sup>29</sup>. That's 123 people every single day. This leads to a situation where often the pharmaceutical industry is a bigger danger to the lives of sick people than the sickness itself. Here’s an example: Hepatitis C is the deadliest infectious disease in the United States. According to the Centers for Disease Control and Prevention, about 2.4 million Americans have got Hepatitis C<sup>30</sup>. In 2018, 15,713 people died because of it<sup>31</sup>. But not a single of these persons would have had to die, because there is a cure. In 2014, the first pill named Sofosbuvir came to market which has often been called a miracle drug. Taking one of these pills every day over a period of 12 weeks heals 97%<sup>32</sup> of patients permanently and the drug doesn't have bad side effects. If everyone infected with Hepatitis C took Sofosbuvir, the sickness could be eradicated completely. So why are people still dying because of Hepatitis C? The reason is the greed of large pharmaceutical companies like Gilead Sciences. A single pill of Sofosbuvir costs 1000$<sup>33</sup>, which makes it the most expensive substance on earth. The cost of producing this drug is less than 1$ per pill<sup>34</sup>, which means that the profit margin for this pill is more than 1000 times larger than the cost of producing it. Gilead Sciences defended itself by arguing that researching and developing the drug was so expensive that it had to charge such high prices. So let's take a look at the development of Sofosbuvir. The fundamental research on this drug was actually done at Emory University by Raymond Schinazi<sup>35</sup>, and the research was publicly funded by taxes<sup>36</sup>. Raymond and a colleague decided to monetize his discovery and started a company called Pharmasset. This company was then quickly bought up by Gilead Sciences for 11.200.000.000$<sup>37</sup>. So Raymond enriched himself by selling his miracle drug even though his research had been paid for by the state. Now Gilead Sciences is holding a patent on the drug and can set the prices as high as they want. The taxpayer is the one who's left out, he actually has to pay twice, he first pays for the research and then for the overpriced drug in case he gets sick. So where does the money for these drugs go? A lof of it goes into the pockets of the executives of these companies, John Martin, CEO of Gilead Sciences made about $232.0 million Dollars in 2015 alone<sup>38</sup>, which was one year after the drug came to market. The top 5 earning executives of the company made 400 million dollars in 2015, this money would be enough to completely eradicate Hepatitits C from the United states and there would still be plenty of money left to get rid of the disease many in other countries<sup>39</sup>. The money that doesn’t end up in the pockets of the executives goes into advertisements. Pharmaceutical companies spend 19 times as much money on advertising than on research & development<sup>40</sup>. 
Having a monopoly for 20 years is very lucrative already, but why stop there when you can get 40 years of patent protection? To maximize their profits, pharmaceutical companies engage in so called "evergreening" techniques, these techniques extend the life-cycle of a patent over the legal 20 years. whenever the patent on a very profitable drug is about to expire, pharmaceutical companies do everything they can to keep their monopoly. To do that, they abuse the legal system. Here's an example: The patent on Prilosec, a drug against heartburn was about to expire, so Astra Zeneca, the company producing this drug simply took the mirror image of the exact same molecule and patented it as a new discovery, thereby giving them an additional 20 years of patent protection, event though there is no difference in the drug and the medicine works the same as it did the previous 20 years, there was no innovation at all<sup>41</sup>. The only thing that’s different is that the drug is now called Nexium instead of Prilosec.

## How Efficient is the Intellectual Property System?
The IP system costs a ton of money, intellectual property lawyers are expensive and don’t add value to th economy, people are sitting in jail right now because they didn't obey copyright law which costs us tax money and some companies have even turned it into their business model to sue other companies who violate patents.
Intellectual property also creates an incentive for authors to spend a lot of their time in legal battles and securing their monopoly instead of inventing and creating. And this is not a new phenomenon, one of the early examples of this is that of Giuseppe Verdi, who was one of the first composers to make extensive use of copyright. This however, lead to him putting less time into composing and more time into legally securing the monopoly over his works, resulting in less creative output<sup>42</sup>.
Additionally, IP encourages authors to create redundant works. For example: There are a lot of books on how to learn programming. Many of them have identical approaches and content. Authors who also want to make a book on the topic have to write a completely new book from scratch because they are not allowed to copy parts from books from other authors. But instead of creating yet another one of these books, it would often be better and more efficient to take an already existing book and improve and modify it, but copyright prevents this. 

Purism is a company which is currently building a smartphone using 100% open source software.
The company’s CTO said that one of the biggest problems with creating a smartphone was not to violate any Patents. To illustrate how extreme the current situation is: there are 250.000 essential patents on the 4G mobile standard alone<sup>43</sup>. Keeping an eye on whether your company violates any of these patents is extremely expensive and costs a lot of resources that could be used for innovating more instead.

## Is Intellectual Property Just?
The line of what is covered by IP and what isn't is drawn completely arbitrary. There is no reason why a theoretical scientist cannot patent his discoveries and an engineer can. Both engage in a creative process and in the end both find out something nobody else has before. This line is constantly being redrawn, but that doesn’t make it any more fair.

What is legal and what is illegal according to copyright is also completely arbitrary. Supporters of IP like to argue that if you want to enjoy great content, you have to pay for it, everything else is stealing because the author has to profit from the fruits of his labor. But what they miss out on is that paying for content does not mean that the author will be rewarded. For example: If I buy a used DVD from eBay, I can now legally watch the movie, but the artist didn't get any money from the sale of that DVD. For him there is no difference between illegally downloading the movie and buying it used. Yet, downloading the movie is illegal even though in both cases the author gets no money. The same thing happens when you go to a friend and watch a movie together which he bought. You don't have to pay to watch the movie, the author doesn't get any money from you and yet what you are doing is legal. Intellectual property laws show many inconsistencies. For example: Online pirates do the same thing public libraries do, but just more efficiently. Libraries lend books, CD's, movies etc. to one person each. Music/movie “Pirates” share education and culture in the form of books, publications, movies and music for free on the internet with millions of people at the same time. Yet what they are doing is illegal while libraries are seen as a social institution and are respected in society. Public libraries cost the state a lot of money while file sharing is free and no money is needed from the state. An interesting point to note is that when the first public libraries were created in the 1850s, publishers protested heavily and said that if public libraries were legal, not a single book would ever be written again since writers wouldn’t be able to make money44. They argued that lending a book without paying was stealing. However, it doesn’t seem like public libraries have destroyed the business model of authors.

A questionable property of the patent system is that it doesn't allow for simultaneous invention. It may come as a surprise to you, but simultaneous inventions are not rare. In fact, they're rather the norm. Often, people who are sitting on the other side of the globe invent the same product at the same time. The history of the radio, which was developed in the end of the 19th century, clearly shows this phenomenon. Guglielmo Marconi is often portrayed as the inventor of the radio. But in fact, Marconi had many competitors who were faster than him. Oliver Lodge, Nicola Tesla, Alexander Popov and Henry B. Jackson. All of these inventors were working on a radio at about the same time, and some had already published their results in scientific papers describing a radio before Marconi. A very similar apparatus "invented" by Marconi had actually already been created before him by Ernest Rutherford. The theoretical research however didn’t come from any of the named persons. It came from physicists like James Clerk Maxwell and Heinrich Rudolf Hertz who discovered and researched so called "Hertz Waves" which can be used for the the transmission of signals over the air using radio waves. Marconi's contribution to the invention of the radio was rather simple: He grounded the antenna and the transmitter. Everything else had already been developed by engineers that were quicker than Marconi. Still, Marconi was able to patent all parts of the radio as his invention, despite the fact that all the crucial components needed for a radio transmitter to work were not developed by him. The reason why Marconi was granted the patent wasn't his genius, it was his wealth. He was born into a rich aristocratic family which meant that he had good connections to London. Marconi's patent meant that the ones who had contributed way more to the invention of the radio than Marconi, were left out. They were now no longer allowed to build radios using the technology they themselves had invented. Legally competing with Marconi and his broad patent was now impossible. Nicola Tesla, who had been working on radios before Marconi had also tried to patent the components he developed, but he didn't have the money and influence Marconi had, which meant that Tesla had to die poor and without recognition of his contribution to the invention of the radio, while Marconi became richer every year using his monopoly. Later the Patent Office of the United States even pointed out that Marconi's patent application was invalid because he patented components that had been developed by Nicola Tesla. So the U.S. Patent Office reverted its decision in 1943 and recognized Tesla’s patent applications as valid and Macroni’s patents as invalid, but at that time Tesla had already died<sup>45</sup>. 
What can we learn from this? Our patent system doesn't allow for simultaneous invention even though it does happen. Inventions are not the product of some great genius but are developed over a long period of time by different people who improve the idea a little step at a time. The patent system only allows one person to be recognized as the inventor and rewards that person with a monopoly while all the others are left out. The person granted the patent usually isn't the inventor who made the biggest improvements to a technology but the one who is rich and has got better connections. And nowadays it's no different. Patents are expensive and independent inventors can't afford expensive patent protection while large monopolistic companies can.  

Nowadays, patents only benefit the richest companies. For small companies, patents make competing with large companies impossible. Large companies often hold so many patents in their field, that for small companies it’s impossible to produce a competing product without infringing on the industry leader’s patents. Because many small businesses realize this, they become “one idea” companies with the goal to be bought up by a large player in the industry, cashing out and feeding the monopolist<sup>46</sup>. Often, getting a patent isn’t even worth it for small companies in the first place, as the cost of filing and enforcing the patent is much larger than the return as a single patent is not enough to protect an idea. To successfully block competitors, a whole patent portfolio has to be filed or bought up, which only the large companies can afford<sup>48</sup>.

One of the most well known cases of injustice in the IP system is that of academic publishers. Scientific papers are usually behind an expensive paywall. Prices for these articles are so high that even Havard Business School has said that they can no longer pay the insane prices charged by publishers like Elsevier and Wiley<sup>48</sup>. What many academics are upset about, is that these publishers don’t pay at all for the creation of their papers. As already said before, universities are publicly funded by taxes, publishers pay neither for the research nor the review of the articles they publish. Yet they hold a monopoly on and profit from the knowledge that should help humanity advance and thrive. Universities fund the articles, but then have to buy back their own creations by paying enormous licensing fees which can range from 100.000 USD to 2.000.000 USD<sup>49</sup>. Academic researchers from universities with less money, especially from developing countries are not allowed to take part in the advancement of science because these paywalls prevent them from accessing articles that they need for their research. The original authors of these articles don’t profit at all from this business model and usually have to give up all of their rights, which means that they don’t even have got the right to copy the papers they themselves have written.<sup>50</sup>

A more philosophical answer to the question whether IP is just or not is that we live in a world where it has almost become impossible to really create something entirely on your own. Artists copy ideas from other artist’s works, musicians are inspired by each other, scientists use other people's theories and research to improve upon them. And most people who use some software to create a creative work aren’t the authors of the software which enables them to express themselves. I wouldn’t have been able to write this article without reading through dozens of other people’s works. Copying information is what has allowed humanity to develop and thrive. Copying is what enables us to not have to start from the beginning over and over again, because if we choose to share we don’t have to do the work someone else has already done and instead use our resources to improve what already exists. Our world has simply become so interdependent that even the simplest things cannot be achieved without using a product, idea or concept that somebody else invented. So how is it fair that only the person who produces the final product should be awarded a monopoly while all the other people are left out?

Finally, most people would probably agree that education should be free for everyone. In the past this wasn't possible because it was expensive to spread information. Nowadays, because of the internet it is possible to share content with millions of people without even leaving your room. There is so much economic and social value in having an educated public since education can be a way out of poverty and increases wealth. But copyright excludes poor people from education who could otherwise learn a lot from video tutorials or educational software.

## The Intellectual Property Industry’s Double Moral Standard
Throughout the whole intellectual property industry we can find a recurring pattern which strongly contradicts the actions by major rightsholders. To illustrate this, let’s take a look at Hollywood. The story begins in New Jersey with Thomas Edison, who was involved in the invention of  motion picture cameras and was very rich. He saw the opportunity to make a lot of money using this new technology and bought up as many patents around this technology as he could and together with important players in the movie industry, he founded a cartel - the "Motion Picture Patents Company".  The MPPC used their patents to monopolize the movie sector. Everyone who wanted to make a movie or show a movie at a theater had to pay hefty fees to the MPPC. Also, they had to use expensive equipment licensed by the MPPC. Using his patents, Edison also made it illegal to watch movies not produced in the U.S by banning all imports of foreign movies. Some of the rules he introduced were completely arbitrary. For example: he made it illegal to produce movies longer than 20 minutes - because he thought that Americans didn’t have a longer attention span. Independent movie makers were upset about Thomas' abuse of the patent system. So filmmakers in New Jersey looked for a way to get around the monopoly of the MPPC and recognized that California was far away enough to be out of reach for the MPPC. So studios like Fox and Paramount moved to Hollywood and were now able to produce movies without having to pay enormous fees<sup>51</sup>. 
So Hollywood only exists because of movie-makers who ran away from intellectual property. Ironically, nowadays Hollywood is one of the strongest supporters of intellectual property and major studios try to sue everyone who illegally "Pirates" and distributes their movies. The industry’s double moral standard becomes even clearer when taking a look at Disney,  a company which always lobbies heavily for an extension of copyright with enormous success. However, Disney itself is a company which profits heavily from copying other peoples ideas. Movies like "Pinocchio" "Sleeping Beauty" and "Snow White and the Seven Dwarfs" all take their story from fairy tales and folklore that were never invented by Disney, but developed over generations<sup>52</sup>.

The manga industry shows similar behavior. For a long time, it was not possible to buy manga in Europe and the USA since they were not licensed and had no translations. So fans who could speak Japanese stepped in and started scanning Japanese manga, translating them and making them available for free on the internet. Only through these so called "Scanlations" was it that manga became popular very quickly in the western world. Distributing these scanlations is illegal because they are not authorized by the copyright holder. Even Japanese manga artists admit that illegally copied manga laid the foundation for the western manga industry. Here is a quote from the Japanese manga author Takahashi "The English manga industry’s foundation was built from scanlation after all". 
What many find to be troubling is that the manga publishers who owe their popularity to manga "pirates" are now going after exactly those, the people that helped them to their success. Publishers constantly try to take down any website which provides free access to copyrighted manga and anime<sup>53</sup>. And it's not just the manga industry.
Spotify is a very good example for the music industry. In the beginning, Spotify used illegally downloaded music from websites like "The Pirate Bay" for its streaming service. Nowadays, Spotify is trying as hard as they can to take down "The Pirate Bay" and other illegal music downloading sites<sup>54</sup> – again, even though it helped them to their success. Even popular musicians in the music industry have started to notice that piracy, the thing they fight  so hard against isn’t that bad of a thing at all. Here’s a quote from Ed Sheeran in an interview with CBS:
“I know that’s a bad thing to say, because I’m part of a music industry that doesn’t like illegal file sharing, but Illegal file sharing was what made me. It was students in England going to university, sharing my songs with each other.”<sup>55</sup>

## Can Artists Make Money Without Intellectual Property?
The most common argument brought up by the music and movie industry is that they need intellectual property, otherwise there wouldn't be any new movies, books or music since creative people wouldn’t be making any money. Before we get into this, I’d like to point out that in this discussion it is often undermined that even if artists were not paid, many would still create content, because it is their passion to create. Humans have a need to express themselves which many do by writing books, poems, plays, making music or art. So it is important to keep in mind that there is something more to art than making money. Also, the intention behind the creation of intellectual property was never to reward creators in the first place, so maybe it isn’t the best system to reward creativity. But of course, artists should get paid so that they can make a living. 
Luckily, it turns out that there are many ways for creative people to make money other than having a monopoly. In many sectors, intellectual property didn't exist for a very long time. Yet, creative people were still rewarded for their work. So we are going to take a closer look at those to see how creative minds can make money without intellectual property. But first of all, authors could still sell their content in the absence of copyright. The only difference is that they would have competition. To explain this, we’ll take a look into the past. 
For a long time there was no copyright in the USA on books from England, yet the authors would still get paid from publishers in exchange for being the first who received the manuscript of the book. In order for the sale of the books to be profitable, American publishers flooded the market as fast as possible at a very low price, because they knew that "Pirates" would be copying the books and compete very quickly. This resulted in books being available very quickly and at low prices while the authors were still rewarded<sup>56</sup>. The same business model could be applied to movies and music nowadays. The author Eric Flint pointed out that about 80% of a book's sales happen in the first three months after its release. As an example he presented the sales of his own book “Oblique Approach” as you can see in the 
table below.

![Booksales Oblique Approach](BooksalesObliqueApproach.png)
<div align="center">[57]</div>

So the business model of American publishers should still be profitable today since most of the revenue comes in very quickly, before imitators come in. Another way of making money is personalized content which adds value to a product that cannot be copied. For example: Many digital artists sell signed prints of their drawings. Let’s say the name of a fan of an artist is Joe. One possibility the artist can personalize the art ist to add a text like “for my fan Joe” above his signature. Together with advertisements, sponsorships, crowdfunding, merchandising and donations it is easily possible to make money without copyright. Also, authors of books can give readings of their book just like musicians give concerts.
Nowadays copyright mainly benefits the people who are already rich. Some stars are able to make a fortune by people streaming their music on streaming services like Spotify. But most musicians don't make money from Spotify, instead they are making money by touring and giving concerts which are way more profitable, so for small musicians copyright is often already obsolete today. And even pop stars admit that. Lady Gaga, who doesn’t have a problem with people “Pirating” her music said the following in an interview with Times Online:
"you know how much you can earn off touring, right? Big artists can make anywhere from $40 million for one cycle of two years' touring. Giant artists make upwards of $100 million. Make music -- then tour. It's just the way it is today."<sup>58</sup>
In 2018, a study at Queens University in Canada even showed that for smaller and mid-tier musicians piracy is beneficial, it increases their revenue since piracy is the fastest way for them to grow their audience<sup>59</sup>.
Giving up their copyright can actually be very beneficial for creators. To prove this, I am going to present an example of an industry that is growing rapidly without copyright: 
In the software industry, a relatively new business model has come up: Open source as a business.
Open source simply means that companies are giving away the source code of their programs and let everyone use it, modify it and distribute it for free. Even though users don’t have to pay for the software, the industry is growing rapidly and open source companies are making billions of dollars every year even though they give up their copyright. Open Source companies don't make money like companies who offer their apps for free and fill them with ads and trackers, open source software is not only free to use but also free of ads or data collection. 
Here are five of the most successful open source projects, you probably know all of them.
LibreOffice, Android, Firefox, Thunderbird and Signal Messenger.
So how do such companies make money and why do they give away their code for free when they could instead have a monopoly.
Firstly, open source companies make a lot money. One of these companies is Red Hat, in this graph you can see how quickly their yearly revenue is growing.

![Yearly Revenue of Red Hat Inc.](RedHatYearlyRevenue.png)
<div align="center">[60]</div>

Another example is LibreOffice, an open source alternative to Microsoft’s office suite using which this article you’re reading right now was written. Different companies are developing LibreOffice together. While the software is free, they make money by offering client companies who use LibreOffice in their business support, if clients encounter any issues or bugs with LibreOffice, they will fix them<sup>61</sup>. And if a client wants a new feature, they will offer to add the feature for a fee. But not only the client and the developers profit from this. The new feature is not just made available to the client, but to everyone. Blender is also a program which is open source. It has become the movie industry's standard for 3D animated movies. The program is funded by donations which bring in enough money to be able to pay for 24 employees creating the program<sup>62</sup>. Big companies such as EPIC games, the creators of Fortnite are willing to donate Blender a lot of money since they use it to create their games. Recently, EPIC games donated 1.200.000$ to support the development of Blender<sup>63</sup> because they know that their contribution makes a difference and that they will profit from their donation by getting an even better program.
Secondly, the open source community helps companies who open source their code and improve it for free. Not a single person has to put all of their work into the development of the software. Instead, people from all over the world contribute a small piece of the code at a time.
Thirdly, free software means free advertisement and allows a program to gain new users rapidly. This means that open source software is more competitive than traditional software, because it's free.

![Mobile Operating Syestem Market Share Worldwide](MobileOperatingSystemMarketShare.png)
<div align="center">[64]</div>

The pie chart above shows the mobile operating system market-share of Android compared to iOS. And as we can see, Android (which is open source) is completely dominating the mobile market while iOS (which is closed source) only manages to capture under a third of the market.
Fourth, using open source software gives clients a guarantee that they can use the program in the future. Microsoft could shut down it’s servers at any time and suddenly nobody would be able to use Microsoft Office any more. To further illustrate the power of giving up intellectual property: Two economists (Bessen and Hunt) analyzed the software industry, and estimate that without software patents, the rate of innovation for software would have been 15% higher<sup>66</sup>.
You may wonder how open source companies like the developers of LibreOffice who sell support for enterprises who use their software stay profitable. Anyone can simply take their code, re-brand the software and sell support for it at a cheaper price. The open source company developing the program has to put in all the money and resources to create a program while other companies who copy the code don’t have to pay the cost for innovating. So one might expect the open source company to go bankrupt since the imitator has a monetary advantage because he has lower expenses. Well, open source companies are imitated all the time, but imitators don’t have an easy time. To explain this, let’s take a look at Red Hat, which is one of the most successful open source companies. In 2019 alone, it made 3.360.700.000$. Nowadays Red Hat charges 349$<sup>66</sup> for a package including customer support for it’s operating system “Red Hat Enterprise Linux” (RHEL). In the beginning, the company had several imitators including Hcidesign and Linuxemporium. In 2002, these companies offered the same services Red Hat provided for a significantly lower price. While Red Hat charged 56.95$ for a package with support for RHEL, its competitors only demanded 16$. So one would expect Red Hat to go out of business, because why would you pay more for the same product? It turns out, Red Hat has sold way more of it’s packages than Hcidesign or Linuxemporium<sup>67</sup>. Why is that? - Think about it, if you download a program and it doesn’t work the way you want or you have a question, who would your rather contact, the developers of the program or some company that simply copied and pasted the code? The company that develops a product is also the one who understands it best. It is always working on the front line and the developers have to understand everything they’re doing which means that they will be much faster and much more efficient at solving their client’s problems than a company which first has to study the code in order to understand it. Because of this, clients prefer to work with the actual developers rather than imitators. As for the two companies mentioned earlier, today they don’t even exist any more while Red Hat’s revenue is skyrocketing.
It’s not just the software industry that has noticed how beneficial a world where information is shared freely can be for everyone. In 2020, the author Jeff Geerling decided to give away his e-books for free because he wanted people to have access to Information during the corona pandemic. He expected this to have a negative impact on his revenue, but the opposite was the case, his sales increased 4 times<sup>68</sup> because making his books available for free was the best form of advertising he could get. 
A great example of an artist who makes money without copyright is David Revoy. David is the creator of the popular “Pepper and Carrot” web-comics. Everyone can read, download, modify and distribute his comics for free without having to ask David for permission. And yet, he earns 3,169$ for every single episode of Pepper and Carrot he produces<sup>69</sup>. His business model is simple: Everyone can read his comics for free, if you like his comics, you can support him with a donation of 1$ per episode of Pepper and Carrot. In return, you will be mentioned in the end of the comic with a thank you from the author. Currently over 1000 people donate for every new episode. The great thing about his comics is that he doesn’t have to advertise them since the content being free is already advertisement on it’s own. Another advantage of his business model is that it encourages creativity: Someone made a Pepper & Carrot short movie, someone else a jump and run game, and someone else a board game using the artwork from Pepper & Carrot. And this is only possible because David doesn't copyright his drawings.
Oh, and a little side note: David only uses open source software to create his comics.

## The Side Effects of Intellectual Property
If intellectual property is supposed to stimulate creation, it needs to enable artist to freely express themselves. Sadly, intellectual property does the exact opposite, it limits artistic freedom. Copyright doesn’t only cover books, but also the characters in the story which means that you are not allowed to use characters from other books in your own book. So if you’re a fan of a book series and have an idea for a sequel, copyright prevents you from writing that book. Some authors hunt down everyone who writes stories containing characters from their books. For example: Sharon Lee and Steve Miller, who are science fiction writers strictly oppose fan fiction. Here is a quote from Sharon: "I don’t want “other people interpreting” our characters. Interpreting our characters is what Steve and I do; it's our job. Nobody else is going to get it right. This may sound rude and elitist, but honestly, it's not easy for us to get it right sometimes, and we’ve been living with these characters. . .for a very long time... We built our universes, and our characters; they are our intellectual property; and they are not toys lying about some virtual sandbox for other kids to pick up and modify at their whim.”<sup>70</sup> The same goes for songs, musicians are not allowed to make remixes or use parts of already existing songs except if they pay immense license fees which go beyond most artist’s budget.
IP also increases the cost of creation. e.g. a youtuber making a video with copyrighted background music has to pay for the right to use the music. Using other people's work in your own can be very costly. ”Tarnation" is a movie from Jonathan Caouette about his life with his mentally ill mother which cost only 218$ to produce. Jonathan was awarded with the "Best Documentary" award from the National Society of Film Critics. Even though the movie was so cheap to produce, licenses for music and short clips used in the movie cost a whopping 400.000$<sup>71</sup>. So copyright increased the cost of creating Jonathan Caouette’s movie by about 2000 times. So while copyright is supposed to give the creator money, it often actually takes it from him.

Copyright also creates the unwanted problem of orphan works. “Orphan Work" is a term for books, music, movies or other creative works that are still copyrighted even though their copyright holder has already died or cannot be contacted. These works are often lost because nobody has got the right to reproduce them and they're no longer being sold. The issue of orphan work has been called “perhaps the single greatest impediment to creating new works”<sup>72</sup>.

The patent system has a very harmful side effect to innovation, so called patent trolls are companies who own patents but don't use them to create a product. Instead, they patent a broad idea and wait until another company invents something that is covered by their patent. Patent trolls then go after companies who "Pirate" their intellectual property and sue them to make enormous amounts of money. An example would be "Shipping and transit LLC" - a company which is now bankrupt but extracted 15 million dollars from other companies through patent trolling. Shipping and transit LLC owns several patents on logistics like tracking a car or a package. For example: U.S. Patent No. 6.415.207. A patent for a “system for monitoring and reporting status of vehicles.” The method used for tracking vehicles is described very vaguely in the patent description and the company didn't invent anything new with this patent. The company’s strategy - like all other patent trolls' - is to threaten small businesses with suing them for insanely high sums of money in court. They then give the companies who infringe on their patent the chance to agree on a settlement for a couple of thousand of dollars. The price for the settlement is usually lower than having to go to court and paying for lawyers. So most businesses agree to settle because it's cheaper than challenging the patent troll in court. 
The work of such companies is completely useless to the economy and doesn't create any benefit for society. The 15.000.000$ which "Shipping and transit LLC" took from other companies who were actually producing products could have been used for research, raising the wages of workers and to develop new products<sup>73</sup>.


## Intellectual Property and its Effect on Freedom of Speech
Copyright is used by companies, governments and individuals to censor unwanted ideas or criticism. The Ecuadorian government is well known for this practice. They abuse the copyright they have on the logo of the government to censor political dissidents<sup>74</sup>. Especially president Rafael Correa has spent millions of dollars to censor online criticism.

## Trademarks
Another form of intellectual property which we haven’t discussed before are trademarks. These however are not controversial as they serve to identify rather than monopolize. Trademarks give customers the insurance that they are buying the product from the company they intended to and not some cheap imitation.

## Conclusion 
After we have taken a closer look at the world of intellectual property, I will leave the conclusion to you, what do you think? Does intellectual property fulfill the points listed below?

1. It should increase innovation. 
2. It should reward people proportionately to how much they innovate.
3. It shouldn’t have dangerous side effects.
4. Everybody should benefit from it, nobody should be excluded.

### What You Can Do to Free the World of Intellectual Property
In case you are now convinced that intellectual property does more harm that good, you can release content you produce e.g. music, images, videos, artwork etc. under a Creative Commons license to ensure that everyone will be able to have access to your content and can improve upon it.

## Sources

[1] **How Long Does Copyright Protection Last? - U.S. Copyright Office**  
https://www.copyright.gov/help/faq/faq-duration.html  
(last accessed  2021-03-21)

[2] **What Can Be Patented: Everything You Need to Know – Upcounsel**  
https://www.upcounsel.com/what-can-be-patented#what-can-be-patented  
(last accessed 2021-03-21)

[3] **How Much Does an International Patent cost: Everything You Need to Know – Upcounsel**
https://www.upcounsel.com/how-much-does-an-international-patent-cost  
(last accessed 2021-03-21)

[4] **Galileo affair (6.3 Copernican Books Banned) - Wikipedia**  
https://en.wikipedia.org/wiki/Galileo_affair  
(last accessed 2021-03-21)

**Index Librorum Prohibitorum (1.1 European restrictions on the right to print) - Wikipedia**  
https://en.wikipedia.org/wiki/Index_Librorum_Prohibitorum  
(last accessed 2021-03-21)

[5] **Berne Convention - Britannica**  
https://www.britannica.com/topic/Berne-Convention  
(last accessed 2021-03-22)

[6] **Article 17 of the Directive on Copyright in the Digital Single Market - Gesellschaft für Freiheitsrechte**  
https://freiheitsrechte.org/home/wp-content/uploads/2020/11/GFF_Article17_Fundamental_Rights.pdf  
(last accessed 2021-03-22)

[7] **Boldrin & Levine: Against Intellectual Monopoly, Chapter 8, p.238**  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-22)

[8] **Boldrin & Levine: Against Intellectual Monopoly, Chapter 3, p.48**  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-22)

[9] **Statute of Monopolies - Wikipedia**  
https://en.wikipedia.org/wiki/Statute_of_Monopolies  
(last accessed 2021-03-22)

[10] **Software patent (3.13 United States) - Wikipedia**  
https://en.wikipedia.org/wiki/Software_patent#United_States  
(last accessed 2021-03-26)

[11] **A Patent Lie – The New York Times**  
https://www.nytimes.com/2007/06/09/opinion/09lee.html  
(last accessed 2021-03-26)

[12] **How Google and Big Tech Killed the U.S. Patent System**  
https://www.ipwatchdog.com/2018/03/21/how-google-and-big-tech-killed-the-u-s-patent-system/id=95080/  
(last accessed 2021-03-27)

[13] **Boldrin & Levine: Against Intellectual Monopoly, Chapter 3, p.69**  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-22)

[14] **Protecting their Intellectual Assets: Approbability Conditions and why U.S Manufacturing Firms Patent (or Not) - National Bureau of Economic Research**  
https://www.nber.org/system/files/working_papers/w7552/w7552.pdf  
(last accessed 2021-03-27)

[15] **The Arms Race – The Economist**  
https://www.economist.com/special-report/2005/10/22/the-arms-race  
(last accessed 2021-03-27)

[16] **Patent Thickets: Strategic Patenting of Complex Technologies – James Bessen**  
https://researchoninnovation.org/thicket.pdf  
(last accessed 2021-03-27)

[17] **Boldrin & Levine: Against Intellectual Monopoly, Chapter 4, p.92-93**  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-22)

[18] **United States Copyright Office Fiscal 2017 Annual Report – United States Copyright Office**  
https://www.copyright.gov/reports/annual/2017/ar2017.pdf  
(last accessed 2021-04-08)
       
**US Population by Year - multpl**  
https://www.multpl.com/united-states-population/table/by-year  
(last accessed 2021-04-08)

From 1909 on the United States Copyright Office began to include non-literary works in it’s registrations. In 2017, 44.5% of registered works were literary. For this graph I assumed that between 1909 and 2017 the fraction of literary works decreased linearly from 100% to 44.5%.
       
[19] **Biological patents in the United States - Wikipedia**  
https://en.wikipedia.org/wiki-Biological_patents_in_the_United_States  
(last accessed 2021-03-27a)

[20] **Boldrin & Levine: Against Intellectual Monopoly, Chapter 3, p.62**  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-22)

[21] **Crop Production Historical Track Records April 2018 - U.S. Department of Agriculture**  
https://www.nass.usda.gov/Publications/Todays_Reports/reports/croptr18.pdf  
(last accessed 2021-03-28)

[22] **Monsanto sued small famers to protect seed patents, report says - The Guardian**  
https://www.theguardian.com/environment/2013/feb/12/monsanto-sues-farmers-seed-patents  
(last accessed 2021-03-28)

[23] **Who Owns the Seed? - Seed Freedom**  
https://seedfreedom.info/who-owns-the-seed/  
(last accessed 2021-03-28)

[24] **Boldrin & Levine: Against Intellectual Monopoly, Chapter 8, p.217**  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-22)

[25] **Boldrin & Levine: Against Intellectual Monopoly, Chapter 9, p.251**  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-22)

[26] **Boldrin & Levine: Against Intellectual Monopoly, Chapter 9, p.257**  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-22)

[27] **Biomedical research in the United States - Wikipedia**  
https://en.wikipedia.org/wiki/Biomedical_research_in_the_United_States  
(last accessed 2021-03-28)

[28] **Until 2014, This Man Was TIME's Only Medical Person of the Year – Time**  
https://time.com/3627996/david-ho-person-of-the-year/  
(last accessed 2021-03-28)

[29] **The Americans dying because they can't afford medical care  - The Guardian**  
https://www.theguardian.com/us-news/2020/jan/07/americans-healthcare-medical-costs  
(last accessed 2021-03-28)

[30] **Hepatitis C Questions and Answers for the Public – Centers for Disease Control and Prevention**  
https://www.cdc.gov/hepatitis/hcv/cfaq.htm  
(last accessed 2021-03-28)

[31] **Viral Hepatitis Surveillance Report 2018 — Hepatitis C**  
https://www.cdc.gov/hepatitis/statistics/2018surveillance/HepC.htm  
(last accessed 2021-03-28)

[32] **Sovaldi 200 mg film-coated tablets - electronic medicines compendium**  
https://www.medicines.org.uk/emc/medicine/28539  
(last accessed 2021-03-28)

[33] **Gilead's $1,000 Pill Could Eradicate Hepatitis C, But Ethical And Financial Challenges Loom - Forbes**  
https://www.forbes.com/sites/matthewherper/2013/12/19/can-gileads-1000-pill-eradicate-hepatitis-c/  
(last accessed 2021-03-28)

[34] **DRUG$: The Price We Pay, 20:23 – AIDS Healthcare Foundation**  
https://www.youtube.com/watch?v=xZ_ncGqyAfY  
(last accessed 2021-03-29)

[35] **Where There's A Cure There's Controversy: Raymond Schinazi's Storyarticle 2 – PharmaIntelligence**  
https://pharmaintelligence.informa.com/resources/product-content/where-there-is-a-a-cure-there-ia-controversy  
(last accessed 2021-03-29)

[36] **Public funding for transformative drugs: the case of sofosbuvir** 
https://pubmed.ncbi.nlm.nih.gov/33011345/  
(last accessed 2021-03-29)

[37] **Gilead Sciences Completes Acquisition of Pharmasset, Inc. - Gilead Sciences** 
https://www.gilead.com/news-and-press/press-room/press-releases/2012/1/gilead-sciences-completes-acquisition-of-pharmasset-inc  
(last accessed 2021-03-29)

[38] **The Mismeasure of Mammon:Uses and Abuses of Executive Pay Data – Institute for New Economic Thinking**  
https://www.ineteconomics.org/uploads/papers/WP_49_Hopkins_Lazonick_August_29.pdf  
(last accessed 2021-03-29) 

[39] **DRUG$: The Price We Pay, 28:45 – AIDS Healthcare Foundation**  
https://www.youtube.com/watch?v=xZ_ncGqyAfY  
(last accessed 2021-03-29)

[40] **Pharmaceutical Companies Spent 19 Times More On Self-Promotion Than Basic Research: Report - Huffpost**  
https://www.huffpost.com/entry/pharmaceutical-companies-marketing_n_1760380  
(last accessed 2021-03-29)

[41] **Nexium: The Dark Side Of Pharma - acsh.org**  
https://www.acsh.org/news/2017/01/18/nexium-dark-side-pharma-10546  
(last accessed 2020-06-10)

[42] **The Emergence of Musical Copyright in Euprope From 1709 to 1850 – F.M. Scherer**  
https://papers.ssrn.com/sol3/papers.cfm?abstract_id=1336802  
(last accessed 2021-04-02)

[43] **A mobile phone that respects your freedom - Chaos Communication Congress, 6:39 – media.ccc.de**  
https://media.ccc.de/v/Camp2019-10238-a_mobile_phone_that_respects_your_freedom  
(last accessed 2020-06-12)

[44] **You Can't Defend Public Libraries and Oppose File-Sharing - Torrent Freak**  
https://torrentfreak.com/you-cant-defend-public-libraries-and-oppose-file-sharing-150510/  
(last accessed 2020-06-07)

[45] **Boldrin & Levine: Against Intellectual Monopoly, Chapter** 8, p.230-233  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-22)

[46] **Boldrin & Levine: Against Intellectual Monopoly, Chapter 4, p.82**  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-29)

[47] **For Most Small Companies Patents Are Just About Worthless – Forbes**  
https://www.forbes.com/sites/toddhixon/2013/10/04/for-most-small-companies-patents-are-just-about-worthless/#341d923916ab)  
(last accessed 2021-03-29)

[48] **Harvard University says it can't afford journal publishers' prices – The Guardian**  
https://www.theguardian.com/science/2012/apr/24/harvard-university-journal-publishers-prices  
(last accessed 2021-04-03)

[49] **How much did your university pay for your journals? - ScienceMag**  
https://www.sciencemag.org/news/2014/06/how-much-did-your-university-pay-your-journals  
(last accessed 2021-04-03)

[50] **The Exploititive Business Model of Academic Publishers Fuels Piracy - Torrentfreak**  
https://torrentfreak.com/the-exploitive-business-model-of-academic-publishers-fuels-piracy-210227/  
(last accessed 2021-04-03)

[51] **The Renegade Roots of Hollywood Studios - History.com**  
https://web.archive.org/web/20180215112359/https://www.history.com/news/the-renegade-roots-of-hollywood-studios  
(last accessed 2021-03-29)

[52] **List of Disney animated films based on fairy tales - Wikipedia**  
https://en.wikipedia.org/wiki/List_of_Disney_animated_films_based_on_fairy_tales  
(last accessed 2021-04-02)

[53] **Manga, Scanlation, and The Curious Case of The ‘Pirating’ Anti-Piracy Advocate - Torrenfreak**  
https://torrentfreak.com/manga-scanlation-and-the-curious-case-of-the-pirating-anti-piracy-advocate-200607/  
(last accessed 2021-03-29)

[54] **Spotify's Beta Used 'Pirate' MP3 Files, Some From Pirate Bay - Torrentfreak**  
https://torrentfreak.com/spotifys-beta-used-pirate-mp3-files-some-from-pirate-bay-170509/  
(last accessed 2021-03-29)

[55] **Ed Sheeran: ‘Illegal File Sharing Is What Made Me” – Digital Music News**  
https://www.digitalmusicnews.com/2017/03/13/ed-sheeran-piracy/  
(last accessed 2021-03-29)

[56] **Boldrin & Levine: Against Intellectual Monopoly, Chapter 2, p.24-29**  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-22)

[57] **Boldrin & Levine: Against Intellectual Monopoly, Chapter 6, p.158**  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-22)

[58] **Lady Gaga Says No Problem If People Download Her Music; The Money Is In Touring – Techdirt**  
https://www.techdirt.com/articles/20100524/0032549541.shtml  
(last accessed 2021-03-29)

[59] **Purchase, Pirate, Publicize: Private-Network Music Sharing and Market Album Sales - Queen’s Economics Department**
https://www.econ.queensu.ca/sites/econ.queensu.ca/files/qed_wp_1354.pdf  
(last accessed 2021-03-29)

[60] **Red Hat Reports Fiscal Fourth Quarter and Fiscal Year 2009 Results – Red Hat Inc.**  
https://www.redhat.com/en/about/press-releases/q4   
https://www.redhat.com/en/about/press-releases/q4fy10
https://www.redhat.com/en/about/press-releases/q411
https://www.redhat.com/en/about/press-releases/red-hat-reports-fourth-quarter-and-fiscal-year-2012-results
https://www.redhat.com/en/about/press-releases/red-hat-reports-fourth-quarter-and-fiscal-year-2013-results
https://www.redhat.com/en/about/press-releases/red-hat-reports-fourth-quarter-and-fiscal-year-2014-results
https://www.redhat.com/en/about/press-releases/red-hat-reports-fourth-quarter-and-fiscal-year-2015-results
https://www.redhat.com/en/about/press-releases/red-hat-reports-fourth-quarter-and-fiscal-year-2016-results
https://www.redhat.com/en/about/press-releases/red-hat-reports-fourth-quarter-and-fiscal-year-2017-results
https://www.redhat.com/en/about/press-releases/red-hat-reports-fourth-quarter-and-fiscal-year-2018-results
https://www.redhat.com/en/about/press-releases/red-hat-reports-fourth-quarter-and-fiscal-year-2019-results
https://www.redhat.com/en/about/press-releases/red-hat-reports-first-quarter-results-fiscal-year-2020  
(last accessed 2021-03-29)

[61] **Deploy LibreOffice professionally and securely! - CIB**  
https://libreoffice.cib.de/#en  
(last accessed 2021-03-29)

[62] **About – Blender**  
https://www.blender.org/about/  
(last accessed 2021-03-29)

[63] **Epic Games supports Blender Foundation with $1.2 million   Epic MegaGrant - Blender**  
https://www.blender.org/press/epic-games-supports-blender-foundation-with-1-2-million-epic-megagrant/  
(last accessed 2021-03-29)

[64] **Mobile Operating System Market Share Worldwide - Statcounter**  
https://gs.statcounter.com/os-market-share/mobile/worldwide  
(last accessed 2021-03-29)

[65] **Boldrin & Levine: Against Intellectual Monopoly, Chapter 4, p.92**  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-29)

[66] **Linux platforms – Red Hat Inc.**  
https://www.redhat.com/en/store/linux-platforms  
(last accessed 2021-03-29)

[67] **Boldrin & Levine: Against Intellectual Monopoly, Chapter 2, p.23-24**  
http://dklevine.com/papers/imbookfinalall.pdf  
(last accessed 2021-03-29)

[68] **I gave away my books for free, and sales increased 4x - Jeff Geerling**
https://www.jeffgeerling.com/blog/2020/i-gave-away-my-books-free-and-sales-increased-4x  
(last accessed 2021-03-29)

[69] **David Revoy - Patreon**  
https://www.patreon.com/davidrevoy  
(last accessed 2021-03-29)

[70]  **Steve Miller (science fiction writer) (2.1 On fan fiction) - Wikipedia**  
https://en.wikipedia.org/wiki/Steve_Miller_(science_fiction_writer)  
(last accessed 2021-03-29)

[71] **Tarnation (2003 film) - Wikipedia**  
https://en.wikipedia.org/wiki/Tarnation_(2003_film)  
(last accessed 2021-03-29)

[72] **Orphan Works and Mass Digitization - a report of the register of copyrights – United States Copyright Office**  
https://www.copyright.gov/orphan/reports/orphan-works2015.pdf  
(last accessed 2021-03-22)

[73] **Stupid Patent of the Month: How 34 Patents Worth $1 Led to Hundreds of Lawsuits**  
https://www.eff.org/deeplinks/2018/10/stupid-patent-month-how-34-patents-worth-1-led-hundreds-lawsuits  
(last accessed 2021-03-27)

[74] **State Censorship by Copyright? Spanish Firm Abuses DMCA to Silence Critics of Ecuador's Government - Electronic Frontier Foundation**  
https://www.eff.org/deeplinks/2014/05/state-censorship-copyright-spanish-firm-abuses-DMCA  
(last accessed 2020-01-03)
 
[75] **Copyright Shouldn't Be A Tool of Censorship - Electronic Frontier Foundation**  
https://www.eff.org/deeplinks/2017/01/copyright-shouldnt-be-tool-censorship  
(last accessed 2020-01-03)

### Further Information

#### Literature
**Against Intellectual Monopoly– Michele Boldrin, David K. Levine**  
Cambridge University Press  
2008

#### Documentary
**DRUG$ - THE PRICE WE PAY**  
Aids Healthcare Foundation    
2018

#### Video
**Why copyright makes no sense | The case against intellectual   property - The Hated One**  
https://www.youtube.com/watch?v=QVkeJI2feyQ  
(last accessed 2020-01-03)

*****



<div align="center">No Copyright 2021 - CopyCat  

Licensed under CC0 1.0 Universal  (CC0 1.0 Public Domain Dedication)  
https://creativecommons.org/publicdomain/zero/1.0/</div>

<div align="center">Contribute to this article on GitLab: 
<a href="https://gitlab.com/CreativeCopyCat/intellectualproperty/-/blob-masterIntellectual%20Property%20and%20its%20effects%20on%20economy%20and%20humanity.md">https://gitlab.com/CreativeCopyCat/intellectualproperty</a> </div>

